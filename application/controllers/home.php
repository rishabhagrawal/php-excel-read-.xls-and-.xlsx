<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller {

	public function index()
	{
		if($this->input->post('submit_btn'))
		{
			set_include_path(get_include_path() . PATH_SEPARATOR . 'php-excel/');

			/** PHPExcel_IOFactory */
			include 'PHPExcel/IOFactory.php';

			$file_name = $_FILES['file']['tmp_name'];
			$inputFileName = $file_name;  // File to read

			//echo 'Loading file ',pathinfo($inputFileName,PATHINFO_BASENAME),' using IOFactory to identify the format<br />';
			try
			{
				$objPHPExcel = PHPExcel_IOFactory::load($inputFileName);
			}
			catch(Exception $e)
			{
				die('Error loading file "'.pathinfo($inputFileName,PATHINFO_BASENAME).'": '.$e->getMessage());
			}

			echo "<pre>";
			$sheetData = $objPHPExcel->getActiveSheet()->toArray(null,true,true,true);
			print_r($sheetData);

			// foreach ($sheetData as $name) {
			// 		foreach ($name as $key => $application) {
			// 			echo $key;
			// 			echo "----";
			// 			echo "<br>";
			// 		}
			// }

			$this->load->view('home');
		}
		else
		{
			$this->load->view('home');
		}
	}
}